import java.util.Scanner;

class sunmask {
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter ip : ");
        String ip = kb.next();
        System.out.print("Enter sub mask : ");
        int mask = kb.nextInt();
        process(ip, mask);
    }

    static public void process(String ip, int mask) {
        String[] str = ip.split("\\.");
        int check = Integer.parseInt(str[0]);
        checkClass(check);
        Mask(mask, str, ip);
        hostNet(mask);

    }

    static public void checkClass(int ip1) {
        if (ip1 > 0 && ip1 <= 127) {
            System.out.println("Class A");
        } else if (ip1 > 127 && ip1 <= 191) {
            System.out.println("Class B");
        } else if (ip1 > 191 && ip1 <= 223) {
            System.out.println("Class C");
        } else if (ip1 > 223 && ip1 <= 239) {
            System.out.println("Class D");
        } else if (ip1 > 239 && ip1 <= 255) {
            System.out.println("Class E");
        }
    }

    static public void Mask(int mask, String[] str, String ip) {

        System.out.print("Subnet : ");
        if (mask > 0 && mask <= 8) {
            mask = 8 - mask;
            mask = Cal(mask);
            int wc = 255 - mask;
            int tp = Integer.parseInt(str[0]);
            int bc = tp + wc;
            if (bc >= 255) {
                bc = 255;
            }
            int n = bc - wc;
            System.out.println(mask + ".0.0.0");
            System.out.println("Wildcard : " + wc + ".255.255.255");
            System.out.println("Network address : " + n + ".0.0.0");
            System.out.println("Broadcast address : " + bc + ".255.255.255");
            System.out.println("HostMin : " + n + ".0.0.1");
            System.out.println("HostMax : " + bc + ".255.255.254");
        } else if (mask > 8 && mask <= 16) {
            mask = 16 - mask;
            mask = Cal(mask);
            int wc = 255 - mask;
            int tp = Integer.parseInt(str[1]);
            int bc = tp + wc;
            if (bc >= 255) {
                bc = 255;
            }
            int n = bc - wc;
            System.out.println("255." + mask + ".0.0");
            System.out.println("Wildcard : 0." + wc + ".255.255");
            System.out.println("Network address : " + str[0] + "." + n + ".0.0");
            System.out.println("Broadcast address : " + str[0] + "." + bc + ".255.255");
            System.out.println("HostMin : " + str[0] + "." + n + ".0.1");
            System.out.println("HostMax : " + str[0] + "." + bc + ".255.254");
        } else if (mask > 16 && mask <= 24) {
            mask = 24 - mask;
            mask = Cal(mask);
            int wc = 255 - mask;
            int tp = Integer.parseInt(str[2]);
            int bc = tp + wc;
            if (bc >= 255) {
                bc = 255;
            }
            int n = bc - wc;
            System.out.println("255.255." + mask + ".0");
            System.out.println("Wildcard : 0.0." + wc + ".255");
            System.out.println("Network address : " + str[0] + "." + str[1] + "." + n + ".0");
            System.out.println("Broadcast address : " + str[0] + "." + str[1] + "." + bc + ".255");
            System.out.println("HostMin : " + str[0] + "." + str[1] + "." + n + ".1");
            System.out.println("HostMax : " + str[0] + "." + str[1] + "." + bc + ".254");
        } else if (mask > 24 && mask <= 32) {
            mask = 32 - mask;
            mask = Cal(mask);
            int wc = 255 - mask;
            int tp = Integer.parseInt(str[3]);
            int bc = tp + wc;
            if (bc >= 255) {
                bc = 255;
            }
            int n = bc - wc;
            System.out.println("255.255.255." + mask);
            System.out.println("Wildcard : 0.0.0." + wc);

            if (mask == 255) {
                System.out.println("Network address : " + ip);
                System.out.println("Broadcast address : " + ip);
            } else {
                System.out.println("Network address : " + str[0] + "." + str[1] + "." + str[2] + "." + n);
                System.out.println("Broadcast address : " + str[0] + "." + str[1] + "." + str[2] + "." + bc);
            }
            System.out.println("HostMin : " + str[0] + "." + str[1] + "." + str[2] + "." + (n + 1));
            System.out.println("HostMax : " + str[0] + "." + str[1] + "." + str[2] + "." + str[3]);
        }else{
                System.out.println("Error mask");
        }
    }
    static public void hostNet(int mask){
        System.out.print("Host/Net : ");
        if(mask==32){
            System.out.println("1.0");
        }else{
        mask = 32-mask;
        System.out.println(Math.pow(2,mask)-2);
        }
    }

    static public int Cal(int sub) {
        int cal = 0;
        for (int i = 7; i >= sub; i--) {
            cal += Math.pow(2, i);
        }
        return cal;
    }
}